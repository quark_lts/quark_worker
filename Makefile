all: build.dev

build_run: build.dev test.run


build.dev:
	cd build &&	go build ../cmd/*.go

test.run: 
	./run.sh 
