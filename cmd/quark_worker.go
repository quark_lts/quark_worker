package main

import (
	"context"
	"flag"
	"sync"

	"github.com/quark_lt/pkg/util/config"
	"gitlab.com/quark_worker/cmd/app_worker"
	"gitlab.com/quark_worker/pkg/quark_logger"
)

var (
	globalCtx, cancel = context.WithCancel(context.Background())
)

/*
Parse File or Load from URL
*/
func ParseDatafile(args string) *config.QuarkLTConfig {
	if len(args) > 0 {
		return config.ParseMainConfig(config.ReadFile(args))
	}
	return config.DownloadFile("http://localhost:7777/")

}

//todo: Add log rotation to file or log.file
func main() {
	fileData := flag.String("f", "", "a string")
	databaseUrl := flag.String("db", "http://localhost:8086", "database url to write metrics")
	noOutput := flag.Int("out", quark_logger.STDOUT_LOGGER, "output to stdout flag")
	flag.Parse()
	quark_logger.SetupLogger(*noOutput)
	instance := app_worker.NewAppWorker(&sync.WaitGroup{}, ParseDatafile(*fileData), *databaseUrl)
	instance.Start()
}
